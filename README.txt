Module: TagMark
Author: Mike Carter <mike@ixis.co.uk>
Sponsor: Lullabot <lullabot.com>


Description
===========
Provides the glue to make a del.icio.us style social bookmarking system.


Requirements
============
Links.module - http://drupal.org/node/24719
Views.module - http://drupal.org/project/views
Links "views" support patch - http://drupal.org/node/52639


Installation
============
Install the module in the usual way, there are no direct mysql tables to import.

The module will create its own taxonomy to store bookmark tags in.


Settings
========
A new setting is provided per content-type. This allows the administrator
to decide what Drupal nodes can be bookmarked. You will need to enable
'bookmarking' on types you want.

The additional link related settings are defined via the Links module settings.

Permissions are available to control who can create and edit bookmarks along with
who can view what users have said about a specific node.


Usage
=====

Users may bookmark site content (that has been allowed) by clicking the
'bookmark this' link displayed with each node.

Once a node has at least one user who has bookmarked it an additional link
will appear that shows how many users have bookmarked the same node. Clicking
this link will list all the users along with their personal notes about the node.

Bookmarking of links outside the site (as del.icio.us does) is provided by the
createcontent->bookmark option. The URL can also be called from a JavaScript
bookmarklet with the parameters 'title' and 'url'.

Example:
http://example.com/?q=node/add/bookmark&url=http://drupal.org/project/views&title=Views+Module
