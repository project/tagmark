<?php
/* tagmark_views.inc,v 1.1 2006/05/23 11:29:20 budda Exp
 * Provide the core views for bookmarking
 */
function tagmark_views_default_views() {
  $view = new stdClass();
  $view->name = 'bookmarks-recent-user';
  $view->description = 'Lists all bookmarks made by a user';
  $view->access = array (
  0 => '2',
);
  $view->view_args_php = '';
  $view->page = TRUE;
  $view->page_title = 'User Bookmarks View';
  $view->page_header = '';
  $view->page_header_format = '1';
  $view->page_footer = '';
  $view->page_footer_format = '1';
  $view->page_empty = 'You haven\'t bookmarked anything yet.';
  $view->page_empty_format = '1';
  $view->page_type = 'node';
  $view->url = 'user/$arg/bookmarks';
  $view->use_pager = TRUE;
  $view->nodes_per_page = '50';
  $view->menu = TRUE;
  $view->menu_title = 'bookmarks';
  $view->menu_tab = TRUE;
  $view->menu_tab_default = FALSE;
  $view->menu_weight = '';
  $view->block = TRUE;
  $view->block_title = 'My Bookmarks';
  $view->block_header = 'You recently added the following pages.';
  $view->block_header_format = '1';
  $view->block_footer = '';
  $view->block_footer_format = '1';
  $view->block_empty = '';
  $view->block_empty_format = '1';
  $view->block_type = 'list';
  $view->nodes_per_block = '3';
  $view->block_more = '1';
  $view->block_use_page_header = FALSE;
  $view->block_use_page_footer = FALSE;
  $view->block_use_page_empty = TRUE;
  $view->sort = array (
    array (
      'tablename' => 'node',
      'field' => 'created',
      'sortorder' => 'DESC',
      'options' => '',
    ),
  );
  $view->argument = array (
    array (
      'type' => 'uid',
      'argdefault' => '1',
      'title' => '',
      'options' => '',
    ),
  );
  $view->field = array (
    array (
      'tablename' => 'node',
      'field' => 'title',
      'label' => '',
      'handler' => 'views_handler_field_nodelink',
      'sortable' => '1',
    ),
    array (
      'tablename' => 'term_node_6',
      'field' => 'name',
      'label' => '',
    ),
    array (
      'tablename' => 'node',
      'field' => 'created',
      'label' => '',
      'handler' => 'views_handler_field_date_small',
      'sortable' => '1',
    ),
  );
  $view->filter = array (
    array (
      'tablename' => 'node',
      'field' => 'type',
      'operator' => 'OR',
      'options' => '',
      'value' => array (
  0 => 'bookmark',
),
    ),
  );
  $view->requires = array(node, term_node_6);
  $views[$view->name] = $view;

    $view = new stdClass();
  $view->name = 'bookmarks-recent';
  $view->description = 'Recently bookmarked URLs';
  $view->access = array (
);
  $view->view_args_php = '';
  $view->page = TRUE;
  $view->page_title = 'Recent Bookmarks';
  $view->page_header = '';
  $view->page_header_format = '1';
  $view->page_footer = '';
  $view->page_footer_format = '';
  $view->page_empty = '';
  $view->page_empty_format = '';
  $view->page_type = 'list';
  $view->url = 'bookmarks/recent';
  $view->use_pager = TRUE;
  $view->nodes_per_page = '50';
  $view->block = TRUE;
  $view->block_title = 'Recently Bookmarked';
  $view->block_header = 'blah blah blah';
  $view->block_header_format = '1';
  $view->block_footer = '';
  $view->block_footer_format = '';
  $view->block_empty = '';
  $view->block_empty_format = '';
  $view->block_type = 'list';
  $view->nodes_per_block = '5';
  $view->block_more = '1';
  $view->block_use_page_header = TRUE;
  $view->block_use_page_footer = FALSE;
  $view->block_use_page_empty = FALSE;
  $view->sort = array (
    array (
      'tablename' => 'node',
      'field' => 'created',
      'sortorder' => 'DESC',
      'options' => '',
    ),
  );
  $view->argument = array (
  );
  $view->field = array (
    array (
      'tablename' => 'node',
      'field' => 'title',
      'label' => '',
      'handler' => 'views_handler_field_nodelink',
    ),
    array (
      'tablename' => 'node',
      'field' => 'created',
      'label' => '',
      'handler' => 'views_handler_field_date_small',
      'defaultsort' => 'DESC',
    ),
    array (
      'tablename' => 'term_node_6',
      'field' => 'name',
      'label' => '',
    ),
  );
  $view->filter = array (
    array (
      'tablename' => 'node',
      'field' => 'type',
      'operator' => 'OR',
      'options' => '',
      'value' => array (
  0 => 'bookmark',
),
    ),
  );
  $view->requires = array(node, term_node_6);
  $views[$view->name] = $view;

    $view = new stdClass();
  $view->name = 'bookmarks-node';
  $view->description = 'List all users who bookmarked the node';
  $view->access = array (
);
  $view->view_args_php = '';
  $view->page = TRUE;
  $view->page_title = 'Users who bookmarked this node';
  $view->page_header = 'Users who bookmarked the page.';
  $view->page_header_format = '1';
  $view->page_footer = '';
  $view->page_footer_format = '1';
  $view->page_empty = '';
  $view->page_empty_format = '1';
  $view->page_type = 'table';
  $view->url = 'node/$arg/bookmarks-view';
  $view->use_pager = TRUE;
  $view->nodes_per_page = '50';
  $view->sort = array (
    array (
      'tablename' => 'node',
      'field' => 'created',
      'sortorder' => 'ASC',
      'options' => '',
    ),
  );
  $view->argument = array (
    array (
      'type' => 'nid',
      'argdefault' => '1',
      'title' => '',
      'options' => '',
    ),
  );
  $view->field = array (
    array (
      'tablename' => 'node',
      'field' => 'title',
      'label' => '',
      'handler' => 'views_handler_field_nodelink',
    ),
  );
  $view->filter = array (
    array (
      'tablename' => 'node',
      'field' => 'type',
      'operator' => 'OR',
      'options' => '',
      'value' => array (
  0 => 'bookmark',
),
    ),
  );
  $view->requires = array(node);
  $views[$view->name] = $view;

    $view = new stdClass();
  $view->name = 'bookmark-popular';
  $view->description = 'Displays most popular bookmarks based on number of clicks';
  $view->access = array (
);
  $view->view_args_php = '';
  $view->page = TRUE;
  $view->page_title = 'Popular Bookmarks';
  $view->page_header = '';
  $view->page_header_format = '1';
  $view->page_footer = '';
  $view->page_footer_format = '1';
  $view->page_empty = '';
  $view->page_empty_format = '1';
  $view->page_type = 'teaser';
  $view->url = 'bookmark/popular';
  $view->use_pager = TRUE;
  $view->nodes_per_page = '50';
  $view->block = TRUE;
  $view->block_title = 'Popular Bookmarks';
  $view->block_header = '';
  $view->block_header_format = '1';
  $view->block_footer = '';
  $view->block_footer_format = '1';
  $view->block_empty = '';
  $view->block_empty_format = '1';
  $view->block_type = 'list';
  $view->nodes_per_block = '5';
  $view->block_more = '1';
  $view->block_use_page_header = FALSE;
  $view->block_use_page_footer = FALSE;
  $view->block_use_page_empty = FALSE;
  $view->sort = array (
    array (
      'tablename' => 'links_node',
      'field' => 'clicks',
      'sortorder' => 'DESC',
      'options' => '',
    ),
  );
  $view->argument = array (
  );
  $view->field = array (
    array (
      'tablename' => 'node',
      'field' => 'title',
      'label' => '',
      'handler' => 'views_handler_field_nodelink',
    ),
  );
  $view->filter = array (
    array (
      'tablename' => 'node',
      'field' => 'type',
      'operator' => 'OR',
      'options' => '',
      'value' => array (
  0 => 'bookmark',
),
    ),
  );
  $view->requires = array(links_node, node);
  $views[$view->name] = $view;


  return $views;
}